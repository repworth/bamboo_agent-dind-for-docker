## Docker container with: 
- Atlassian Bamboo Agent
- Docker in Docker

## FAQ

Q: **Why DInD instead of [Pass-Thru Docker](http://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/) ?**

A: **This allows bind-mounted volumes inside builds, which is not possible in Pass-Thru Docker**


## Example
```shell
docker \
  run \
  -d \
  --privileged \  
  --restart=always \
  -e ATLASSIAN_SUBDOMAIN \
  -e BAMBOO_SECURITY_TOKEN \
  -e AGENT_UUID \
  -e AGENT_NAME \
  docker-bamboo_agent:docker1.9-bamboo_agent
```