#!/bin/bash

if [ -z "${ATLASSIAN_SUBDOMAIN}" ]; then
	echo "Please set ATLASSIAN_SUBDOMAIN environment variable." >&2
	exit 1
fi

if [ -z "${BAMBOO_SECURITY_TOKEN}" ]; then
	echo "Please set BAMBOO_SECURITY_TOKEN environment variable." >&2
	exit 1
fi

if [ ! -z "${AGENT_UUID}" ]; then
/usr/bin/tee /opt/bamboo/agent/bamboo-agent.cfg.xml <<-EOF
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<configuration>
<buildWorkingDirectory>/opt/bamboo/agent/xml-data/build-dir</buildWorkingDirectory>
<agentUuid>${AGENT_UUID}</agentUuid>
<agentDefinition>
<id>1</id>
<name>${AGENT_NAME:=Docker Remote Agent}</name>
<description>${AGENT_DESCRIPTION:=The Bamboo remote agent running in a docker container}</description>
</agentDefinition>
</configuration>
EOF
fi

echo "Starting Docker Daemon..."
nohup docker daemon -p /var/run/docker.pid &> /var/log/docker.log &

echo "Downloading agent JAR..."
curl -sSL https://${ATLASSIAN_SUBDOMAIN}.atlassian.net/builds/agentServer/agentInstaller/ -o /opt/bamboo/bamboo-cloud-agent.jar

echo "Starting Bamboo Agent..."
java -Dbamboo.home=/opt/bamboo/agent -jar /opt/bamboo/bamboo-cloud-agent.jar https://${ATLASSIAN_SUBDOMAIN}.atlassian.net/builds/agentServer/ -t ${BAMBOO_SECURITY_TOKEN}

exec "$@"